#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

fn sqrsum1 (x1: int, y1: int): int = x1 * x1 + y1 * y1
val s = sqrsum1 (1,2)
val _void_ = print s

val _void_ = print '\n'

fn abs (x: double): double = if x >= 0.0 then x else ~x

val _void_ = print(abs 12.0)
val _void_ = print '\n'
//recursive functions

fun sum1 (n: int): int = if n >= 1 then sum1 (n-1) + n else 0

val _void_ = print (sum1 12)
val _void_ = print '\n'


// XXX: this segfaults
(*
fun sum3
  (m: int, n: int): int =
  if m <=n then let
    val mn2 = (m+n)/2 in sum3(m, mn2+1) + mn2 + sum3 (mn2+1, n)
end else 0

val s3 = sum3(12,134)
//val _void_ = print (sum3(14,23))
*)


val _void_ = print '\n'


(* ****** ****** *)
implement main0 () = () // a dummy implementation for [main]
(* ****** ****** *)

(* end of [mytest.dats] *)
