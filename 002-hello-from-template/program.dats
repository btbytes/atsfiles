#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"


val _void_ = print ("Hello, program 002!\n")

val PI = 3.14 and radius = 10.0
val area = PI * radius * radius

val area2 = let
    val PI = 3.14 and radius = 10.0 in
    PI * radius * radius
end

val xyz = ('A', 1, 2.0)
val x = xyz.0 and y = xyz.1 and z = xyz.2
val (x,y,z) = xyz

typedef point2D = @{ x=double, y=double}
val theOrigin = @{ x=0.0, y=0.0 } : point2D
//extract only selected elements
//val @{ x= theOrigin_X, ... } = theOrigin


//sequence expressions
val _void_ = begin
    print 'h'; print 'e'; print 'l'; print 'l'; print 'o'; print '\n';
end

// Chapter 3.
// functions

//XXX: let val x = 3.14 * (10.0 - 1.0 / 1.4142) in x * x end

fn square (x: double): double = x * x

val square =  lam (y: double): double => y * y


fn are_of_a_ring (R: double, r: double): double = 3.1416 * (square(R) - square(r))

//XXX: fn sqrsum1 (x1: int, y1: int): int = x1 * x1 + y1 * y1

// Function interface - note the `fun` keyword as opposed to `fn`

//XXX: fun succ_int (x: int): int

// evaluation of function calls


fn abs(x: double): double =  if (x >= 0) then x else ~x


// Datatypes
datatype intopt =
  | intopt_none of () | intopt_some of (int)


datatype wday =
    | Monday of ()
    | Tuesday of ()
    | Wednesday of ()
    | Thursday of ()
    | Friday of ()
    | Saturday of ()
    | Sunday of ()
// end of [wday]


//fin
val _void_ = print ("End of program.\n")

(* ****** ****** *)
implement main0 () = () // a dummy implementation for [main]
(* ****** ****** *)

(* end of [mytest.dats] *)
