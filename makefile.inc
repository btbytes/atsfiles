##
##
## A Makefile template for compiling ATS programs
##
##

######

PATSUSRQ=$(PATSHOME)
ifeq ($(PATSUSRQ),"")
PATSUSRQ="/usr"
endif # end of [ifeq]

######

PATSCC=$(PATSUSRQ)/bin/patscc
PATSOPT=$(PATSUSRQ)/bin/patsopt
PATSCCFLAGS=
# '-flto' enables link-time optimization such as inlining lib functions
# '-O2' optimization flag
#PATSGCFLAG=-D_ATS_GCATS
# to enable GC at run-time

default: program

program: program.dats
	$(PATSCC) $(PATSGCFLAG) $(PATSCCFLAGS) -o $@ $< || touch $@

distclean:: ; $(RMF) foo

# %_sats.o: %.sats
#   $(PATSCC) $(PATSCCFLAGS) -c $< || touch $@

# %_dats.o: %.dats
#   $(PATSCC) $(PATSCCFLAGS) -c $< || touch $@

RMF=rm -f

clean:
	$(RMF) program
	$(RMF) *~
	$(RMF) *_?ats.o
	$(RMF) *_?ats.c

distclean:: clean

.PHONY: clean
